External Console is general purpose debugging console and that allow you to remotely connect to any software or game that has our Log Server integrated.

Entire system is separated to 2 modules the Log Server and External Console.
<br>
Log Server is used to send logs to client and receive commands from it.
<br>
External Console is standalone executable used to connect to Log Servers.


It is build using C# and uses UDP to send data between the server and client, also we tried to not bloat it too much (so it uses .Net sockets for networking), so the only external library it uses is [gui.cs](https://github.com/migueldeicaza/gui.cs) to provide better UI for External Console.

## External libraries

Big thanks to [Miguel de Icaza](https://github.com/migueldeicaza) for making [gui.cs](https://github.com/migueldeicaza/gui.cs), "console-based user interface toolkit for .NET applications" available under MIT License.