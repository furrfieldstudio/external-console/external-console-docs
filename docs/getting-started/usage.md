# Usage

Entire system is separated to 2 modules the Log Server and External Console.
<br>
Log Server is used to send logs to client and receive commands from it.
<br>
External Console is standalone executable used to connect to Log Servers.

## Unity

Currently External Console only works in playmode and runtime, we are planning to add editor runtime support in the future.

How to start and connect to Log Server:

- Run ExternalConsole.exe
- Enter playmode
- In the unity console you will get information what command write to connect to Log Server. It's written like this ``console connect {CommandsClientPort} {LogServerPort}``
- Now you are ready to view the logs and send commands to Log Server

## C&#35;

//TODO